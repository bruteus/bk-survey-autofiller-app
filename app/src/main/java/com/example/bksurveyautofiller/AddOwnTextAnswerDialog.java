package com.example.bksurveyautofiller;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class AddOwnTextAnswerDialog extends DialogFragment {
    public interface AddOwnTextAnswerDialogListener {
        void onDialogPositiveClick(DialogFragment dialog, String answer);
    }

    AddOwnTextAnswerDialogListener listener;
    public EditText inputAnswer;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (AddOwnTextAnswerDialogListener) context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_add_own_text_answer, null);
        inputAnswer = v.findViewById(R.id.editTextOwnTextAnswer);

        builder.setView(v)
                .setTitle("Wprowadzanie własnej odpowiedzi")
                .setPositiveButton(R.string.saveAnswerButtonText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onDialogPositiveClick(AddOwnTextAnswerDialog.this, AddOwnTextAnswerDialog.this.getAnswer());
                    }
                })
                .setNegativeButton(R.string.cancelButtonText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AddOwnTextAnswerDialog.this.getDialog().cancel();
                    }
                });

        return builder.create();
    }

    private String getAnswer() {
        return inputAnswer.getText().toString();
    }
}
