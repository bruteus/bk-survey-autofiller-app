package com.example.bksurveyautofiller;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.DialogFragment;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Fade;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Scroller;
import android.widget.TextView;

import com.example.bksurveyautofiller.exceptions.UnexpectedHTMLException;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.SocketTimeoutException;
import java.sql.Timestamp;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements AddOwnTextAnswerDialog.AddOwnTextAnswerDialogListener {
    public static final String EXTRA_RECEIPT_CODE = "com.example.bksurveyautofiller.EXTRA_RECEIPT_CODE";
    public static final String EXTRA_GRADE_TYPE = "com.example.bksurveyautofiller.EXTRA_GRADE_TYPE";
    public static final String EXTRA_SOUP = "com.example.bksurveyautofiller.EXTRA_SOUP";
    public static final String EXTRA_COOKIE_STORE = "com.example.bksurveyautofiller.EXTRA_COOKIE_STORE";
    public static final String EXTRA_REASON_SURVEY_FINISHED = "com.example.bksurveyautofiller.EXTRA_REASON_SURVEY_FINISHED";
    public static final String EXTRA_RESULT_CODE = "com.example.bksurveyautofiller.EXTRA_RESULT_CODE";

    public static final Integer REQUEST_CODE_SURVEY_FILLING = 1;

    public enum SurveyGradeType { GRADE_BEST, GRADE_RANDOM }

    private EditText CN1;
    private EditText CN2;
    private EditText CN3;
    private EditText CN4;
    final private int maxLengthCN1 = 6;
    final private int maxLengthCN2 = 4;
    final private int maxLengthCN3 = 6;
    final private int maxLengthCN4 = 1;

    private ProgressBar progressBar;
    private Button buttonStart;
    private RadioButton radioButtonBest;
    private RadioButton radioButtonRandom;
    private ImageView imageViewBKLogo;
    private Button buttonAddOwnTextAnswer;
    private String ownTextAnswer;

    private TextView textViewIPAddress;
    private String IPAddress;
    private Button buttonRecheckIP;

    private SQLiteDatabase database;

    private final TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if(CN1.hasFocus() && CN1.getText().length() == maxLengthCN1)
                CN2.requestFocus();
            else if(CN2.hasFocus() && CN2.getText().length() == maxLengthCN2)
                CN3.requestFocus();
            else if(CN3.hasFocus() && CN3.getText().length() == maxLengthCN3)
                CN4.requestFocus();
            else if(CN4.hasFocus() && CN4.getText().length() == maxLengthCN4) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                CN4.clearFocus();
            }
        }
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
        @Override
        public void afterTextChanged(Editable editable) {}
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageViewBKLogo = findViewById(R.id.imageViewBKLogoSmall);

        Fade fade = new Fade();
        View decor = getWindow().getDecorView();
        fade.excludeTarget(decor.findViewById(R.id.action_bar_container), true);
        fade.excludeTarget(android.R.id.statusBarBackground, true);
        fade.excludeTarget(android.R.id.navigationBarBackground, true);
        getWindow().setEnterTransition(fade);
        getWindow().setExitTransition(fade);

        CN1 = findViewById(R.id.editTextCN1);
        CN2 = findViewById(R.id.editTextCN2);
        CN3 = findViewById(R.id.editTextCN3);
        CN4 = findViewById(R.id.editTextCN4);

        CN1.addTextChangedListener(textWatcher);
        CN2.addTextChangedListener(textWatcher);
        CN3.addTextChangedListener(textWatcher);
        CN4.addTextChangedListener(textWatcher);

        progressBar = findViewById(R.id.progressBar);
        buttonStart = findViewById(R.id.buttonStart);
        buttonAddOwnTextAnswer = findViewById(R.id.buttonAddOwnTextAnswer);
        buttonAddOwnTextAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment fragment = new AddOwnTextAnswerDialog();
                fragment.show(getSupportFragmentManager(), "dupa");
            }
        });

        radioButtonBest = findViewById(R.id.radioButtonBestGrade);
        radioButtonRandom = findViewById(R.id.radioButtonRandomGrade);

        textViewIPAddress = findViewById(R.id.textViewIPAddress);
        textViewIPAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                System.out.println("Entered checking ip");
                if(IPAddress != null) {
                    Timestamp ts = IPTools.getIPLastUsedDate(database, IPAddress);
                    if(ts != null && IPTools.isOlderThanMonth(ts))
                            textViewIPAddress.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, getDrawable(R.drawable.ic_ip_not_used), null);
                    else if(ts != null && !IPTools.isOlderThanMonth(ts))
                            textViewIPAddress.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, getDrawable(R.drawable.ic_ip_used), null);
                    else
                        textViewIPAddress.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, getDrawable(R.drawable.ic_ip_not_used), null);
                }
            }
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable) {}
        });

        buttonRecheckIP = findViewById(R.id.buttonRecheckIP);
        new IPCheckAsyncTask(this).execute();
        buttonRecheckIP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initiateIPCheck();
            }
        });


        AutoFillerDBHelper dbHelper = new AutoFillerDBHelper(this);
        database = dbHelper.getWritableDatabase();
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String answer) {
        ownTextAnswer = answer;
        System.out.println("Own text answer: " + ownTextAnswer);
    }

    private void initiateIPCheck() {
        new IPCheckAsyncTask(this).execute();
    }

    private static class IPCheckAsyncTask extends AsyncTask<Void, Void, Exception> {
        private WeakReference<MainActivity> activityWeakReference;
        private String IPAddress;

        IPCheckAsyncTask(MainActivity activity) {
            activityWeakReference = new WeakReference<>(activity);
        }

        @Override
        protected void onPreExecute() {
            MainActivity activity = activityWeakReference.get();
            if(activity == null || activity.isFinishing())
                return;

            activity.buttonRecheckIP.setEnabled(false);
            activity.textViewIPAddress.setText("Sprawdzanie...");
            activity.textViewIPAddress.setCompoundDrawablesRelative(null,null,null,null);
        }

        @Override
        protected Exception doInBackground(Void... voids) {
            try {
                IPAddress = IPTools.checkIPAddress();
            } catch (IOException | UnexpectedHTMLException e) {
                return e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Exception e) {
            MainActivity activity = activityWeakReference.get();
            if(activity == null || activity.isFinishing())
                return;

            activity.buttonRecheckIP.setEnabled(true);

            if(e != null) {
                activity.IPAddress = null;
                activity.textViewIPAddress.setText("Wystąpił błąd");
            }
            else {
                activity.IPAddress = IPAddress;
                activity.textViewIPAddress.setText(IPAddress);
            }
        }
    }

    private static class SurveyEntryAsyncTask extends SurveyAsyncTask<HashMap<String, String>, Void, Exception> {
        private WeakReference<MainActivity> activityWeakReference;
        private HashMap<String, String> receiptCode;

        SurveyEntryAsyncTask(MainActivity activity) {
            activityWeakReference = new WeakReference<>(activity);
            cookieStore = new HashMap<>();
        }

        private void enableWidgets(boolean enable) {
            MainActivity activity = activityWeakReference.get();
            if(activity == null || activity.isFinishing())
                return;

            activity.buttonStart.setEnabled(enable);
            activity.radioButtonRandom.setEnabled(enable);
            activity.radioButtonBest.setEnabled(enable);
            activity.CN1.setEnabled(enable);
            activity.CN2.setEnabled(enable);
            activity.CN3.setEnabled(enable);
            activity.CN4.setEnabled(enable);
            activity.buttonAddOwnTextAnswer.setEnabled(enable);
            activity.buttonRecheckIP.setEnabled(enable);
        }

        @Override
        protected void onPreExecute() {
            enableWidgets(false);

            MainActivity activity = activityWeakReference.get();
            if(activity == null || activity.isFinishing())
                return;

            activity.progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Exception doInBackground(HashMap<String, String>... data) {
            final String baseURL = "https://www.wizytawburgerking.pl/";
            String nextURL;

            receiptCode = data[0];

            try {
                sendRequest(baseURL, new HashMap<String, String>());
                nextURL = getNextURL();

                HashMap<String, String> params = new HashMap<>();
                params.put("JavaScriptEnabled", "1");
                params.put("FIP", "True");
                params.put("AcceptCookies", "Y");
                params.put("NextButton", "Kontynuuj");

                sendRequest(baseURL + nextURL, params);

                if(soup.selectFirst("form input[id=CN1]") == null)
                    return new UnexpectedHTMLException("Could not find survey's entry point", soup.toString());

                nextURL = getNextURL();

                params.clear();
                params.put("JavaScriptEnabled", "1");
                params.put("FIP", "True");
                params.put("NextButton", "Start");
                params.putAll(receiptCode);  // Receipt code from CN* EditTexts

                sendRequest(baseURL + nextURL, params);
            } catch (IOException | UnexpectedHTMLException e) {
                return e;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Exception e) {
            enableWidgets(true);

            MainActivity activity = activityWeakReference.get();
            if(activity == null || activity.isFinishing())
                return;

            activity.progressBar.setVisibility(View.GONE);

            if(e != null) {
                if(e instanceof SocketTimeoutException)
                    CustomToast.showToast(activity,"Przekroczono limit czasu połączenia", CustomToast.ToastType.ERROR);
                else if(e instanceof UnexpectedHTMLException)
                    CustomToast.showToast(activity,"Programista znowu coś zjebał.\nSkontaktuj się z nim przez linuxxxmaster@protonmail.com", CustomToast.ToastType.ERROR);
                else
                    CustomToast.showToast(activity,"Wystąpił problem z połączeniem", CustomToast.ToastType.ERROR);

                e.printStackTrace();
                return;
            }

            if(soup.selectFirst("div.Error") != null) {
                CustomToast.showToast(activity, "Podany kod jest nieprawidłowy", CustomToast.ToastType.ERROR);
                return;
            }

            Intent intent = new Intent(activity, SurveyFillingActivity.class);

            intent.putExtra(EXTRA_RECEIPT_CODE, receiptCode);

            if(activity.radioButtonBest.isChecked())
                intent.putExtra(EXTRA_GRADE_TYPE, SurveyGradeType.GRADE_BEST);
            else
                intent.putExtra(EXTRA_GRADE_TYPE, SurveyGradeType.GRADE_RANDOM);

            intent.putExtra(EXTRA_SOUP, soup.toString());
            intent.putExtra(EXTRA_COOKIE_STORE, cookieStore);

            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, activity.imageViewBKLogo, ViewCompat.getTransitionName(activity.imageViewBKLogo));
            activity.startActivityForResult(intent, REQUEST_CODE_SURVEY_FILLING, options.toBundle());
        }
    }

    public void startSurvey(View view) {
        if(CN1.getText().length() != maxLengthCN1
                || CN2.getText().length() != maxLengthCN2
                || CN3.getText().length() != maxLengthCN3
                || CN4.getText().length() != maxLengthCN4)
        {
            CustomToast.showToast(this,"Wypełnij wszystkie pola!", CustomToast.ToastType.WARNING);
            return;
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("CN1", CN1.getText().toString());
        params.put("CN2", CN2.getText().toString());
        params.put("CN3", CN3.getText().toString());
        params.put("CN4", CN4.getText().toString());

        new SurveyEntryAsyncTask(this).execute(params);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(data == null)
            return;

        if(requestCode == REQUEST_CODE_SURVEY_FILLING) {
            if(resultCode == RESULT_CANCELED) {
                String message = data.getStringExtra(EXTRA_REASON_SURVEY_FINISHED);
                CustomToast.showToast(this, message, CustomToast.ToastType.ERROR);
            }
            else if(resultCode == RESULT_OK) {
                String valCode = data.getStringExtra(EXTRA_REASON_SURVEY_FINISHED);
                Intent intent = new Intent(this, ResultsActivity.class);

                intent.putExtra(EXTRA_RESULT_CODE, valCode);
                startActivity(intent);
            }
        }
    }
}
