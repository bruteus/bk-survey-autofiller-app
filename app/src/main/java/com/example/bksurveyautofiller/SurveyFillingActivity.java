package com.example.bksurveyautofiller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.transition.Fade;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.example.bksurveyautofiller.exceptions.UnexpectedHTMLException;
import com.example.bksurveyautofiller.questions.PostCodeQuestion;
import com.example.bksurveyautofiller.questions.Question;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;

public class SurveyFillingActivity extends AppCompatActivity {
    private HashMap<String, String> receiptCode;
    private MainActivity.SurveyGradeType gradeType;

    private Button buttonCancel;
    private ProgressBar progressBar;

    private SurveyFillAsyncTask asyncTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_filling);

        Fade fade = new Fade();
        View decor = getWindow().getDecorView();
        fade.excludeTarget(decor.findViewById(R.id.action_bar_container), true);
        fade.excludeTarget(android.R.id.statusBarBackground, true);
        fade.excludeTarget(android.R.id.navigationBarBackground, true);
        getWindow().setEnterTransition(fade);
        getWindow().setExitTransition(fade);

        buttonCancel = findViewById(R.id.buttonCancel);
        progressBar = findViewById(R.id.progressBarSurveyFilling);

        Intent intent = getIntent();
        receiptCode = (HashMap<String, String>) intent.getSerializableExtra(MainActivity.EXTRA_RECEIPT_CODE);

        gradeType = (MainActivity.SurveyGradeType) intent.getSerializableExtra(MainActivity.EXTRA_GRADE_TYPE);
        Document soup = Jsoup.parse(intent.getStringExtra(MainActivity.EXTRA_SOUP));
        HashMap<String, String> cookieStore = (HashMap<String, String>) intent.getSerializableExtra(MainActivity.EXTRA_COOKIE_STORE);

        SurveyData surveyData = new SurveyData(soup, gradeType, cookieStore);
        asyncTask = new SurveyFillAsyncTask(this);
        asyncTask.execute(surveyData);
    }

    private final class SurveyData {
        Document soup;
        MainActivity.SurveyGradeType gradeType;
        HashMap<String, String> cookieStore;

        SurveyData(Document s, MainActivity.SurveyGradeType gT, HashMap<String, String> cS) {
            soup = s;
            gradeType = gT;
            cookieStore = cS;
        }
    }

    private static class SurveyFillAsyncTask extends SurveyAsyncTask<SurveyData, Integer, Exception> {
        private WeakReference<SurveyFillingActivity> activityWeakReference;

        SurveyFillAsyncTask(SurveyFillingActivity activity) {
            activityWeakReference = new WeakReference<>(activity);
        }

        private Integer getProgressStatus() throws UnexpectedHTMLException {
            if(soup == null)
                throw new UnexpectedHTMLException("Empty HTML", null);

            if(soup.selectFirst("div#ProgressPercentage") == null)
                throw new UnexpectedHTMLException("Could not find ProgressPercentage div", soup.toString());

            if(soup.selectFirst("div#ProgressPercentage").text().isEmpty())
                throw new UnexpectedHTMLException("Empty ProgressPercentage div", soup.toString());

            String percent = soup.selectFirst("div#ProgressPercentage").text();
            percent = percent.substring(0, percent.indexOf("%"));
            return Integer.parseInt(percent);
        }

        @Override
        protected Exception doInBackground(SurveyData... surveyData) {
            cookieStore = surveyData[0].cookieStore;
            soup = surveyData[0].soup;

            final String baseURL = "https://www.wizytawburgerking.pl/";
            String nextURL;
            Question question;
            HashMap<String, String> answer;

            try {
                do {
                    if(isCancelled())
                        return null;

                    nextURL = getNextURL();
                    question = Question.getQuestion(soup);
                    answer = question.answer(surveyData[0].gradeType == MainActivity.SurveyGradeType.GRADE_RANDOM ? true : false);
                    System.out.println("Answer:");
                    for(Map.Entry e: answer.entrySet())
                        System.out.println(e.getKey().toString() + ": " + e.getValue().toString());

                    publishProgress(getProgressStatus());

                    sendRequest(baseURL + nextURL, answer);
                } while(!(question instanceof PostCodeQuestion));
            } catch (UnexpectedHTMLException | IOException | NullPointerException e) {
                return e;
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            SurveyFillingActivity activity = activityWeakReference.get();
            if(activity == null || activity.isFinishing())
                return;

            activity.progressBar.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Exception e) {
            SurveyFillingActivity activity = activityWeakReference.get();
            if(activity == null || activity.isFinishing())
                return;

            activity.progressBar.setProgress(100);

            Intent resultIntent = new Intent();
            String resultMessage;
            int resultCode;

            if(e != null) {
                if(e instanceof UnexpectedHTMLException || e instanceof NullPointerException)
                    resultMessage = "Programista znowu coś zjebał.\nSkontaktuj się z nim przez linuxxxmaster@protonmail.com";
                else if(e instanceof SocketTimeoutException)
                    resultMessage = "Przekroczono limit czasu połączenia";
                else if(e instanceof IOException)
                    resultMessage = "Wystąpił problem z połączeniem";
                else
                    resultMessage = "Wystąpił nieznany błąd";

                e.printStackTrace();
                resultCode = RESULT_CANCELED;
            }
            else {
                String valCode = soup.selectFirst("p.ValCode").text();
                resultMessage = valCode.substring(valCode.length() - 5);
                resultCode = RESULT_OK;
            }

            resultIntent.putExtra(MainActivity.EXTRA_REASON_SURVEY_FINISHED, resultMessage);
            activity.setResult(resultCode, resultIntent);
            activity.finish();
        }

        @Override
        protected void onCancelled(Exception e) {
            SurveyFillingActivity activity = activityWeakReference.get();
            if(activity == null || activity.isFinishing())
                return;

            Intent resultIntent = new Intent();
            resultIntent.putExtra(MainActivity.EXTRA_RECEIPT_CODE, activity.receiptCode);
            resultIntent.putExtra(MainActivity.EXTRA_GRADE_TYPE, activity.gradeType);
            resultIntent.putExtra(MainActivity.EXTRA_REASON_SURVEY_FINISHED, "Anulowano wypełnianie ankiety");
            activity.setResult(RESULT_CANCELED, resultIntent);
            activity.finish();
        }
    }

    public void cancelTask(View view) {
        asyncTask.cancel(true);
    }
}
