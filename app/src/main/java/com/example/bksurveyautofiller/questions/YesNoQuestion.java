package com.example.bksurveyautofiller.questions;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

public class YesNoQuestion extends Question {
    public String name;
    public String desc;
    public LinkedHashMap<String, String> opts;

    YesNoQuestion(Document soup) {
        super(soup);

        name = soup.selectFirst("input[type=radio]").attr("name");
        desc = soup.selectFirst("tr[class*=InputRow]").selectFirst("td").text();
        opts = new LinkedHashMap<>();
        for(Element e: soup.selectFirst("tr").select("td.Scale")) {
            String label = e.text();
            String value = e.id().substring(e.id().length() - 1);
            opts.put(label, value);
        }
    }

    @Override
    public HashMap<String, String> answer(boolean randomAns) {
        String value = "";

        if(randomAns) {
            value = getRandomAnswer(opts);
        } else {
            if (name.equals("R041000"))
                value = "2";
            else if (name.equals("R049000"))
                value = "1";
            else {
                value = getRandomAnswer(opts);
            }
        }

        HashMap<String, String> ans = new HashMap<>();
        ans.put(name, value);
        ans.putAll(super.answer(randomAns));
        return ans;
    }
}
