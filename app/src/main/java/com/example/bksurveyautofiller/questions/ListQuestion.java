package com.example.bksurveyautofiller.questions;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

public class ListQuestion extends Question {
    public LinkedHashMap<String, String> names;
    public ArrayList<LinkedHashMap<String, String>> opts;

    ListQuestion(Document soup) {
        super(soup);

        names = new LinkedHashMap<>();
        for(Element e: soup.select("select")) {
            String name = e.attr("name");
            String label = e.prependElement("label").text();
            names.put(name, label);
        }

        opts = new ArrayList<>();
        for(Element e: soup.select("select")) {
            LinkedHashMap<String, String> list = new LinkedHashMap<>();
            for(Element i: e.select("option")) {
                if(i.hasAttr("selected"))
                    continue;
                String label = i.text();
                String value = i.attr("value");
                list.put(label, value);
            }
            opts.add(list);
        }
    }

    @Override
    public HashMap<String, String> answer(boolean randomAns) {
        HashMap<String, String> ans = new HashMap<>();
        ArrayList<String> choices = new ArrayList<>();
        for(HashMap<String, String> hm: opts) {
            Random random = new Random();
            int choice = random.nextInt(hm.size());
            int i = 0;
            for(Map.Entry e: hm.entrySet()) {
                if(i++ == choice) {
                    choices.add(e.getValue().toString());
                    break;
                }
            }
        }

        int i = 0;
        for(Map.Entry e: names.entrySet())
            ans.put(e.getKey().toString(), choices.get(i++));

        ans.putAll(super.answer(randomAns));
        return ans;
    }
}
