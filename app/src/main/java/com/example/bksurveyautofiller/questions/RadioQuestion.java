package com.example.bksurveyautofiller.questions;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

public class RadioQuestion extends Question {
    public String name;
    public String desc;
    public LinkedHashMap<String, String> opts;

    RadioQuestion(Document soup) {
        super(soup);

        name = soup.selectFirst("input[type=radio]").attr("name");
        desc = soup.selectFirst("div.rbListContainer").parent().ownText();
        opts = new LinkedHashMap<>();
        for(Element e: soup.select("input[type=radio]")) {
            String label = e.parent().parent().selectFirst("label").text();
            String value = e.attr("value");
            opts.put(label, value);
        }
    }

    @Override
    public HashMap<String, String> answer(boolean randomAns) {
        String value = "";
        if(randomAns) {
            value = getRandomAnswer(opts);
        } else {
            if (name.equals("R001000")) {
                Random random = new Random();
                value = random.nextInt(2) == 0 ? "2" : "3";
            } else if (name.equals("R000256"))
                value = "2";
            else if (name.equals("R002000"))
                value = "1";
            else
                value = getRandomAnswer(opts);
        }

        HashMap<String, String> ans = new HashMap<>();
        ans.put(name, value);
        ans.putAll(super.answer(randomAns));
        return ans;
    }
}
