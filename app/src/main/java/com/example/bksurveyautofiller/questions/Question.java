package com.example.bksurveyautofiller.questions;

import org.jsoup.nodes.Document;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;


public class Question {
    public String IoNF;
    public String PostedFNS;

    Question(Document soup) {
        IoNF = soup.selectFirst("input[id=IoNF]").val();
        PostedFNS = soup.selectFirst("input[id=PostedFNS]").val();
    }

    public HashMap<String, String> answer(boolean randomAns) {
        HashMap<String, String> ans = new HashMap<>();
        ans.put("IoNF", IoNF);
        ans.put("PostedFNS", PostedFNS);
        return ans;
    }

    @Override
    public String toString() {
        return "IoNF: " + IoNF + "\nPostedFNS: " + PostedFNS + "\n";
    }

    public static Question getQuestion(Document soup) {
        if(soup.selectFirst("form div.inputtyperblv") != null)
            return new RadioQuestion(soup);
        else if(soup.selectFirst("form table.HighlySatisfiedNeitherDESC") != null
                || soup.selectFirst("form table.HighlyLikelyDESC") != null)
            return new TableQuestion(soup);
        else if(soup.selectFirst("form table.YesNoASC") != null)
            return new YesNoQuestion(soup);
        else if(soup.selectFirst("form textarea") != null)
            return new TextQuestion(soup);
        else if(soup.selectFirst("form div.cataList") != null)
            return new CheckBoxQuestion(soup);
        else if(soup.selectFirst("form select") != null)
            return new ListQuestion(soup);
        else if(soup.selectFirst("form input[id=S076000]") != null)
            return new PostCodeQuestion(soup);

        return null;
    }

    protected static String getRandomAnswer(LinkedHashMap<String, String> options) {
        Random random = new Random();
        int choice = random.nextInt(options.size());
        int i = 0;
        for(Map.Entry e : options.entrySet()) {
            if (i++ == choice)
                return e.getValue().toString();
        }
        return null;
    }
}
