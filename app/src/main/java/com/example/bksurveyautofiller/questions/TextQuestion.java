package com.example.bksurveyautofiller.questions;

import org.jsoup.nodes.Document;

import java.util.HashMap;

public class TextQuestion extends Question {
    public String name;
    public String desc;

    TextQuestion(Document soup) {
        super(soup);

        name = soup.selectFirst("textarea").attr("name");
        desc = soup.selectFirst("form label").text();
    }

    @Override
    public HashMap<String, String> answer(boolean randomAns) {
        // TODO: Make file with a lot of possible responses and pick them randomly or download random crap from the net
        HashMap<String, String> ans = new HashMap<>();
        ans.put(name, "Dobre jedzenie");
        ans.putAll(super.answer(randomAns));
        return ans;
    }
}
