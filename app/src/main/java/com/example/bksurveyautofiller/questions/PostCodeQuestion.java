package com.example.bksurveyautofiller.questions;

import org.jsoup.nodes.Document;

import java.util.HashMap;

public class PostCodeQuestion extends Question {
    public final String name = "S076000";

    PostCodeQuestion(Document soup) {
        super(soup);
    }

    @Override
    public HashMap<String, String> answer(boolean randomAns) {
        HashMap<String, String> ans = new HashMap<>();
        ans.put(name, "");
        ans.putAll(super.answer(randomAns));
        return ans;
    }
}
