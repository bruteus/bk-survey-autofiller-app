package com.example.bksurveyautofiller.questions;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

public class TableQuestion extends Question {
    public LinkedHashMap<String, String> names;
    public String desc;
    public LinkedHashMap<String, String> opts;

    TableQuestion(Document soup) {
        super(soup);

        names = new LinkedHashMap<>();
        for(Element e: soup.select("tr[class*=InputRow]")) {
            String name = e.select("input").attr("name");
            String label = e.child(0).text();
            names.put(name, label);
        }

        if(soup.selectFirst("div.blocktitle") == null)
            desc = "";
        else
            desc = soup.selectFirst("div.blocktitle").text();

        opts = new LinkedHashMap<>();
        for(Element e: soup.selectFirst("tr").select("td.Scale")) {
            String label = e.text();
            String value = e.id().substring(e.id().length() - 1);
            opts.put(label, value);
        }
    }

    @Override
    public HashMap<String, String> answer(boolean randomAns) {
        HashMap<String, String> ans = new HashMap<>();

        if(randomAns) {
            ArrayList<String> choices = new ArrayList<>(opts.values());
            Random random = new Random();
            for(Map.Entry e: names.entrySet())
                ans.put(e.getKey().toString(), choices.get(random.nextInt(choices.size())));
        }
        else {
            for(Map.Entry e: names.entrySet())
                ans.put(e.getKey().toString(), "5");
        }

        ans.putAll(super.answer(randomAns));
        return ans;
    }
}
