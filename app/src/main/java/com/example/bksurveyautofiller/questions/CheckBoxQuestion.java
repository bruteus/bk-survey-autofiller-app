package com.example.bksurveyautofiller.questions;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

public class CheckBoxQuestion extends Question {
    public LinkedHashMap<String, String> names;
    public String desc;

    CheckBoxQuestion(Document soup) {
        super(soup);

        names = new LinkedHashMap<>();
        for(Element e: soup.select("div.cataOption")) {
            String name = e.selectFirst("input").attr("name");
            String label = e.text().trim();
            names.put(name, label);
        }

        desc = soup.selectFirst("div.blocktitle").text();
    }

    @Override
    public HashMap<String, String> answer(boolean randomAns) {
        String name = "";

        HashMap<String, String> allowed = new HashMap<>(names);
        allowed.remove("R000090");  // Options I want to exclude
        allowed.remove("R000091");
        allowed.remove("R000096");

        Random random = new Random();
        int choice = random.nextInt(allowed.size());
        int i = 0;
        for(Map.Entry e: allowed.entrySet()) {
            if(i++ == choice) {
                name = e.getKey().toString();
                break;
            }
        }

        HashMap<String, String> ans = new HashMap<>();
        ans.put(name, "1");
        ans.putAll(super.answer(randomAns));
        return ans;
    }
}
