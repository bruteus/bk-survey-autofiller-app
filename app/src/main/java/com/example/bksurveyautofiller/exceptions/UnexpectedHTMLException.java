package com.example.bksurveyautofiller.exceptions;

public class UnexpectedHTMLException extends Exception {
    private String cause;
    private String problematicHTML;

    public UnexpectedHTMLException(String c, String pH) {
        cause = c;
        problematicHTML = pH;
    }

    @Override
    public String toString() {
        return "Cause: " + cause + "\n";
    }

    public void printProblematicHTML() {
        if(problematicHTML == null)
            System.out.println("Problematic HTML either not supplied or is null.");
        else
            System.out.print(problematicHTML);
    }
}
