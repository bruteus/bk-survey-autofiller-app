package com.example.bksurveyautofiller;

import android.app.Activity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

final class CustomToast {
    public enum ToastType { INFO, WARNING, ERROR }

    static void showToast(Activity activity, String message, ToastType type) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup) activity.findViewById(R.id.toastRoot));

        TextView toastText = layout.findViewById(R.id.toastText);
        toastText.setText(message);

        ImageView toastIcon = layout.findViewById(R.id.toastIcon);

        if(type == ToastType.INFO) {
            layout.setBackground(activity.getDrawable(R.drawable.shape_toast_info));
            toastIcon.setImageDrawable(activity.getDrawable(R.drawable.ic_toast_info));
        }
        else if(type == ToastType.ERROR) {
            layout.setBackground(activity.getDrawable(R.drawable.shape_toast_error));
            toastIcon.setImageDrawable(activity.getDrawable(R.drawable.ic_toast_error));
        }
        else if(type == ToastType.WARNING) {
            layout.setBackground(activity.getDrawable(R.drawable.shape_toast_warning));
            toastIcon.setImageDrawable(activity.getDrawable(R.drawable.ic_toast_warning));
        }

        Toast toast = new Toast(activity.getApplicationContext());
        toast.setGravity(Gravity.TOP, 0, 64);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }
}
