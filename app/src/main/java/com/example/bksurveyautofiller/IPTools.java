package com.example.bksurveyautofiller;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.bksurveyautofiller.AutoFillerContract.IPStatsEntry;
import com.example.bksurveyautofiller.exceptions.UnexpectedHTMLException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class IPTools {
    private final static String IP_CHECK_WEBSITE_ADDRESS = "http://ip4.host";
    private final static int TIMEOUT = 10000;
    private IPTools() {}

    public static String checkIPAddress() throws IOException, UnexpectedHTMLException {
        Document soup = Jsoup.connect(IP_CHECK_WEBSITE_ADDRESS).timeout(TIMEOUT).get();
        Element ipAddress = soup.selectFirst("p.ipaddress");
        if(ipAddress == null || !ipAddress.hasText())
            throw new UnexpectedHTMLException("Could not find IP address paragraph or it was empty", soup.toString());

        return ipAddress.text();
    }

    public static Timestamp getIPLastUsedDate(SQLiteDatabase db, String ip) {
        String selection = IPStatsEntry.COLUMN_IP + " = \"" + ip + "\"";
        Cursor cursor = db.query(IPStatsEntry.TABLE_NAME, null, selection, null, null, null, null);

        if(cursor.moveToNext()) {  // We should only have one or zero rows in query result as IP is the primary key
            String time = cursor.getString(cursor.getColumnIndex(IPStatsEntry.COLUMN_LAST_USED));
            Timestamp ts = Timestamp.valueOf(time);
            cursor.close();
            return ts;
        }

        return null;
    }

    public static void addIPToDatabase(SQLiteDatabase db, String ip) {
        ContentValues values = new ContentValues();
        values.put(IPStatsEntry.COLUMN_IP, ip);
        db.insert(IPStatsEntry.TABLE_NAME, null, values);
        System.out.println("Added new address: " + ip);
    }

    public static void updateIPInDatabase(SQLiteDatabase db, String ip) {
        Timestamp ts = new Timestamp(new Date().getTime());
        ContentValues values = new ContentValues();
        values.put(IPStatsEntry.COLUMN_LAST_USED, ts.toString());

        String selection = IPStatsEntry.COLUMN_IP + " = \"" + ip + "\"";
        db.update(IPStatsEntry.TABLE_NAME, values, selection, null);
        System.out.println("IP " + ip + " last used date updated to " + ts.toString());
    }

    public static boolean isOlderThanMonth(Timestamp ts) {
        Date ipLastUsedDate = new Date(ts.getTime());
        Calendar today = Calendar.getInstance();
        Calendar ipLastUsed = Calendar.getInstance();
        ipLastUsed.setTime(ipLastUsedDate);

        System.out.println("Today: " + today.getTime());
        System.out.println("IP last used: " + ipLastUsed.getTime());
        return today.get(Calendar.MONTH) - ipLastUsed.get(Calendar.MONTH) < 1;
    }
}
