package com.example.bksurveyautofiller;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.bksurveyautofiller.AutoFillerContract.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class AutoFillerDBHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "autofiller.db";
    private static final int DATABASE_VERSION = 1;

    private Context dbContext;

    public AutoFillerDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        dbContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_TABLE_TEXT_ANSWERS = "CREATE TABLE IF NOT EXISTS " +
                TextAnswersEntry.TABLE_NAME + " (" +
                TextAnswersEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TextAnswersEntry.COLUMN_ANSWER + " TEXT NOT NULL, " +
                TextAnswersEntry.COLUMN_TIMES_USED + " INTEGER NOT NULL DEFAULT 0);";

        final String SQL_CREATE_TABLE_IP_STATS = "CREATE TABLE IF NOT EXISTS " +
                IPStatsEntry.TABLE_NAME + " (" +
                IPStatsEntry.COLUMN_IP + " TEXT PRIMARY KEY NOT NULL, " +
                IPStatsEntry.COLUMN_LAST_USED + " INTEGER NOT NULL DEFAULT CURRENT_TIMESTAMP);";

        ArrayList<String> answers = new ArrayList<>();
        BufferedReader reader = null;
        try {
            final String TEXT_ANSWERS_ASSET_FILE_NAME = "text_answers";
            reader = new BufferedReader(new InputStreamReader(dbContext.getAssets().open(TEXT_ANSWERS_ASSET_FILE_NAME)));
            String line;
            while((line = reader.readLine()) != null)
                answers.add(line);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(reader != null)
                    reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        db.beginTransaction();
        try {
            db.execSQL(SQL_CREATE_TABLE_TEXT_ANSWERS);
            db.execSQL(SQL_CREATE_TABLE_IP_STATS);

            ContentValues values = new ContentValues();
            for(String ans: answers) {
                values.put(TextAnswersEntry.COLUMN_ANSWER, ans);
                db.insert(TextAnswersEntry.TABLE_NAME, null, values);
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.beginTransaction();
        db.execSQL("DROP TABLE IF EXISTS " + TextAnswersEntry.TABLE_NAME + ";");
        db.execSQL("DROP TABLE IF EXISTS " + IPStatsEntry.TABLE_NAME + ";");
        db.endTransaction();

        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int i, int i1) {
        onUpgrade(db, i, i1);
    }
}
