package com.example.bksurveyautofiller;

import android.os.AsyncTask;

import com.example.bksurveyautofiller.exceptions.UnexpectedHTMLException;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;

public class SurveyAsyncTask<T1, T2, T3> extends AsyncTask<T1, T2, T3> {
    protected HashMap<String, String> cookieStore;
    protected Document soup;

    @Override
    protected T3 doInBackground(T1... t1s) {
        return null;
    }

    protected final void sendRequest(String url, HashMap<String, String> params) throws IOException {
        Connection conn = Jsoup.connect(url);
        conn.data(params);
        conn.cookies(cookieStore);
        conn.timeout(10000);

        soup = conn.post();
        cookieStore.putAll(conn.response().cookies());
    }

    protected final String getNextURL() throws UnexpectedHTMLException {
        if(soup == null)
            throw new UnexpectedHTMLException("Empty HTML", null);

        if(soup.selectFirst("form") == null
                || !soup.selectFirst("form").hasAttr("action"))
            throw new UnexpectedHTMLException("Could not find action attribute of the form", soup.toString());

        return soup.selectFirst("form").attr("action");
    }
}
