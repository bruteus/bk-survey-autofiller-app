package com.example.bksurveyautofiller;

import android.provider.BaseColumns;

public class AutoFillerContract {
    private AutoFillerContract() {}

    public static final class TextAnswersEntry implements BaseColumns {
        public static final String TABLE_NAME = "textAnswers";
        public static final String COLUMN_ANSWER = "answer";
        public static final String COLUMN_TIMES_USED = "timesUsed";
    }

    public static final class IPStatsEntry implements BaseColumns {
        public static final String TABLE_NAME = "ipStats";
        public static final String COLUMN_IP = "ip";
        public static final String COLUMN_LAST_USED = "lastUsed";
    }
}
