package com.example.bksurveyautofiller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ResultsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        Intent intent = getIntent();
        String resultCode = intent.getStringExtra(MainActivity.EXTRA_RESULT_CODE);

        ((TextView) findViewById(R.id.textViewResultCode)).setText(resultCode);
        CustomToast.showToast(this, "Ankieta wypełniona pomyślnie", CustomToast.ToastType.INFO);
    }
}
